<!---
    Copyright 2013, Michigan State University, Board of Trustees

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    Released by Nick Kwiatkowski (nk@msu.edu), as an acting agent of MSU
--->

<cfif (isDefined("url.code") AND (url.state EQ session.user.state))>
    <!--- we are assuming that we are getting this callback from MSU.  --->
    <cfif session.user.processLogin(url.state, url.code)>
        <cflocation url="index.cfm" addtoken="false">
    </cfif>
</cfif>

<h1>There was an error processing your login.  Please try again or contact the MSU Helpdesk at 517-432-6200 for more information</h1>