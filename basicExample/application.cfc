<!---
    Copyright 2013, Michigan State University, Board of Trustees

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    Released by Nick Kwiatkowski (nk@msu.edu), as an acting agent of MSU
--->
<cfcomponent>

    <!--- The following settings are based on your own application.  The session timeout should be no longer
          than what your D6501 application is allowed for.  This is reasonably 30 minutes. --->

    <cfset this.name = "DemoOAuthApplication">
    <cfset this.sessionmanagement = "true">
    <cfset this.sessiontimeout = "#createTimespan(0,0,30,0)#">

    <!--- Application Control Functions --->

    <cffunction name="onSessionStart" access="public" returnType="void" output="false">
        <!--- this is run when the session is first created --->
        <cfset session.user = createObject("component","cfcs.userAuth")>
        <cfset session.user.init()>
    </cffunction>

    <cffunction name="onRequestStart" access="public" returntype="boolean" output="false">
        <!--- this is run on every request.  we will need to check to see if the user is logged in.  If not, we will
        redirect them to the MSU login page. --->

        <cfargument name="TargetPage" type="string" required="true"/>
        <cfset publicPages = "/oauth/index.cfm,/oauth/logoff.cfm,/oauth/callback.cfm">      <!--- this should include your callback page --->

        <cfif listFindNoCase(publicPages, arguments.TargetPage) EQ 0>
            <!--- we are visiting a non-public page --->
            <cfset session.user.forceLogin()>
        </cfif>

        <cfreturn true>
    </cffunction>

</cfcomponent>
